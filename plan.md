## Mission Generation

- generate rooms
- generate people (fighting parties)
- simulate until somebody is hurt

## Gameplay

- start rescue mission
- get to VIP (tactical combat)
- extract (medical minigame)

## Company policy

- least property damage
- no lethal force
- faster = better
- few casualties

